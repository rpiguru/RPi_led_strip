"""
RPi/LED strip project for Parkinsons patients.
This is to help with the patients walk or gait.

"""
import time

from led_strip import LEDStrip
import utils

# Constant values to be changed
led_length = 5      # The length in cm between 2 LEDs in the LED strip
color_r = 255       # RGB color to be displayed
color_b = 0
color_g = 0


class Gaiter:

    strip = None
    pixel_step = 0
    pixel_count = 96

    def __init__(self, pixel_count=96, height=180):
        """
        Initialize this class with given height of patient.
        :param height: Height in cm.
        """
        self.pixel_count = pixel_count
        self.strip = LEDStrip(pixel_count=pixel_count)
        self.pixel_step = utils.height_to_step(height=height) / led_length

    def start(self, interval=1):
        """
        Start LED stripping
        :return:
        """
        print 'Starting... Press CTRL+C to exit'
        cur_pos = 0
        while True:
            if cur_pos >= self.pixel_count:
                self.strip.cls()
                cur_pos = 0
                time.sleep(2)
            else:
                self.strip.set_pixel_color(cur_pos, self.strip.get_color(color_r, color_b, color_g))
                self.strip.write_strip()
                cur_pos += self.pixel_step

            time.sleep(interval)

if __name__ == '__main__':

    # There are 32 pixels in a meter, so let us test with 3m LED strip.
    a = Gaiter(pixel_count=96, height=180)

    a.start(interval=2)
